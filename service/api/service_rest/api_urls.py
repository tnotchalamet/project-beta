from django.urls import path

from .views import (
    api_technicians,
    api_appointments,
    api_finish_appointment,
    api_cancel_appointment,
    automobile_vo_request,
    api_appointment_details
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>/", api_appointment_details, name="api_appointment_details"),
    path("appointments/<int:pk>/cancel/", api_cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", api_finish_appointment, name="finish_appointment"),
    path("automobiles/", automobile_vo_request, name="automobile_vo_request")
]
