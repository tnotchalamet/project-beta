function ListSalespeople(props) {
return (
    <div className="my-1">
    <h1 className="display-5 fw-bold">Salespeople</h1>
  <table className="table table-striped">
    <thead>
      <tr>
        <th>Employee Id</th>
        <th>Name</th>
      </tr>
    </thead>
    <tbody>
      {props.salespeople.map(salesperson => {
        return (
          <tr key={ salesperson.id }>
            <td>{ salesperson.employee_id }</td>
            <td>{ salesperson.first_name} { salesperson.last_name }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
</div>
);
}

export default ListSalespeople
