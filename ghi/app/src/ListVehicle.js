import {useEffect, useState} from "react";

function ListVehicles() {
    const [models, setModels] = useState([]);

    const getData = async () => {
        const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setModels(data.models)
            }
        };

        useEffect(() => {
            getData()
        }, []);

        return(
            <div className="card m-2 p-2">
            <h5>Models</h5>
                <table className="table">
                    <thead>
                        <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Manufacturer</th>
                        <th scope="col">Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map (model => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td><img src={model.picture_url} className="w-25" alt="model pic"/></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }

export default ListVehicles
