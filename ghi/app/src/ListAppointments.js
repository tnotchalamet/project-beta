import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";


function ListAppointments(props) {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');

    console.log(props.automobiles)

    const navigate = useNavigate();
    const refreshPage = () => {
        navigate(0);
    }


    const handleVinChange = (event) => {
        const vin = event.target.value;
        setVin(vin)
    }


    function handleSubmit(event) {
        event.preventDefault();
        const filteredAppointments = props.appointments.filter(appointment => {
            return appointment.vin === vin
        });
        setAppointments(filteredAppointments);
    }

    const handleFinished = async (event) => {
        const id = event.target.id;
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`
        const fetchConfig = {
                method: "put",
                body:  JSON.stringify({
            status: "Finished",
            }),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
        }
    }

    const handleCancelled = async (event) => {
        const id = event.target.id;
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({
        status: "Cancelled",
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            refreshPage();
        }
        }


    useEffect(() => {
    }, [props.appointments]);


    return(
        <div className="m-2">
        <h5>Appointments</h5>

            <form onSubmit={handleSubmit} className="form-inline">
                <div className="input-group mb-3">
                    <input onChange={handleVinChange} value={vin} type="text" className="form-control" placeholder="VIN" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                        <div className="input-group-append">
                            <button className="btn btn-success" type="submit">Submit</button>
                        </div>
                </div>
            </form>

            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">VIN</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Cust. Status</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Technician</th>
                    <th scope="col">Reason</th>
                    <th scope="col">Appt. Status</th>
                    </tr>
                </thead>
                <tbody>
                    {props.appointments.map(appointment => {
                        const formattedDate = new Date(appointment.date_time).toLocaleDateString();
                        const formattedTime = new Date(appointment.date_time).toLocaleTimeString();
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{props.automobiles.sold ? 'YES' : 'NO' }</td>
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appointment.first_name} {appointment.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                                <td><button id={appointment.id} onClick={handleFinished} className="btn btn-success">Finished</button></td>
                                <td><button id={appointment.id} onClick={handleCancelled} className="btn btn-danger">Cancelled</button></td>
                            </tr>
                        )
                        })}
                </tbody>
            </table>
        </div>
    )
}

export default ListAppointments
