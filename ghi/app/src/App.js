import { BrowserRouter, Routes, Route } from 'react-router-dom';
import {useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ListAutomobiles from './ListAutomobiles';
import CreateAutomobile from './CreateAutomobile';
import ListManufacturers from './ListManufacturer';
import CreateManufacturer from './CreateManufacturer';
import ListVehicles from './ListVehicle';
import CreateVehicle from './CreateVehicle';
import ListCustomers from './ListCustomers';
import CreateCustomer from './CreateCustomer';
import ListSalespeople from './ListSalespeople';
import CreateSalespeople from './CreateSalespeople';
import ListSales from './ListSales';
import ListSalespersonHistory from './SalespeopleHistory';
import CreateSale from './CreateSale';
import CreateAppointment from './CreateAppointment';
import ListAppointments from './ListAppointments';
import CreateTechnician from './CreateTechnician';
import ListTechnicians from './ListTechnicians';

function App(props) {
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setVehicles] = useState([]);
  const [autos, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [sales, setSales] = useState([]);
  const [appointments, setAppointments] = useState([]);
  //add state management here

  async function getAppointments() {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
      const { appointments } = await response.json();
        setAppointments(appointments)
    }
};

  async function getManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
     const { manufacturers } = await response.json();
    setManufacturers(manufacturers);
    } else {
    console.error('Error fetching')
  }
   }
   async function getVehicles() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
     const { models } = await response.json();
    setVehicles(models);
    }}

   async function getAutomobiles() {
    const response = await fetch('http://localhost:8100/api/automobiles/')
    ;
    if (response.ok) {
     const { autos } = await response.json()
    setAutomobiles(autos)
    } else {
    console.error('Error fetching')
  }
   }
   async function getCustomers() {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
       const { customers } = await response.json();
      setCustomers(customers);
      } else {
      console.error('Error fetching')
    }

  } catch(e) {
    console.log(e)
  }
}

  async function getSalespeople() {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
     const data = await response.json();
     const salespeople  = data.salespersons
    setSalespeople(salespeople);
    } else {
    console.error('Error fetching')
  }}

  async function getSales() {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
     const { sales } = await response.json();
    setSales(sales);
    } else {
    console.error('Error fetching')
  }}


  //  add functions here

  useEffect(() => {
    getManufacturers();
    getVehicles();
    getAutomobiles();
    getCustomers();
    getSalespeople();
    getSales();
    getAppointments();
    // add appropriate state effect here
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles">
            <Route index element={<ListAutomobiles automobiles={autos} getAutomobiles={getAutomobiles}/>} />
            <Route path="new" element={<CreateAutomobile vehicles={models} getVehicles={getVehicles} manufacturers={manufacturers} getManufacturers={getManufacturers} automobiles={autos} getAutomobiles={getAutomobiles}/>} />
            </Route>
            <Route path="vehicles">
            <Route index element={<ListVehicles vehicles={models} getVehicles={getVehicles}/>} />
            <Route path="new" element={<CreateVehicle manufacturers={manufacturers} getManufacturers={getManufacturers} vehicles={models} getVehicles={getVehicles}/>} />
            </Route>
            <Route path="manufacturers">
            <Route index element={<ListManufacturers manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
            <Route path="new" element={<CreateManufacturer manufacturers={manufacturers} getManufacturers={getManufacturers}/>} />
          </Route>
          <Route path="customers">
            <Route index element={<ListCustomers customers={customers} getCustomers={getCustomers} />} />
            <Route path="new" element={<CreateCustomer customers={customers} getCustomers={getCustomers}/>} />
          </Route>
          <Route path="salespeople">
            <Route index element={<ListSalespeople salespeople={salespeople} getSalespeople={getSalespeople} />} />
            <Route path="new" element={<CreateSalespeople salespeople={salespeople} getSalespeople={getSalespeople}/>} />
            <Route path="history" element={<ListSalespersonHistory sales={sales} getSales={getSales} salespeople={salespeople} getSalespeople={getSalespeople}/> }/>
          </Route>
          <Route path="sales">
            <Route index element={<ListSales sales={sales} getSales={getSales}/>} />
            <Route path="new" element={<CreateSale sales={sales} getSales={getSales} salespeople={salespeople} customers={customers} getCustomers={getCustomers} automobiles={autos} getAutomobiles={getAutomobiles}/>} />
            </Route>
          <Route path="appointments">
            <Route path="list" element={<ListAppointments automobiles={autos} getAutomobiles={getAutomobiles} appointments={appointments} getAppointments={getAppointments} />} />
            <Route path="new" element={<CreateAppointment/>} />
          </Route>
          <Route path="technicians">
            <Route path="list" element={<ListTechnicians/>} />
            <Route path="new" element={<CreateTechnician/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
