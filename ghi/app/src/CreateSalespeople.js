import {useEffect, useState} from "react";

function CreateSalespeople({ getSalespeople }) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');
    const [submitted, setSubmitted] = useState(false)

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
          first_name,
          last_name,
          employee_id,
        };


      const Url = 'http://localhost:8090/api/salespeople/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        }
      }


       const response = await fetch(Url, fetchConfig);
        if (response.ok) {
          setFirstName('');
          setLastName('');
          setEmployeeId('');
          setSubmitted(true);
          getSalespeople();
        }
      }


    function handleChangeFirstName(event) {
      const { value } = event.target;
      setFirstName(value);
    }

    function handleChangeLastName(event) {
        const { value } = event.target;
        setLastName(value);
      }

      function handleChangeEmployeeId(event) {
        const { value } = event.target;
        setEmployeeId(value);
      }


    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salespeople-form">
              <div className="form-floating mb-3">
                <input value={first_name} onChange={handleChangeFirstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={last_name} onChange={handleChangeLastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employee_id} onChange={handleChangeEmployeeId} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee Id</label>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
            {( submitted &&
            <div className="alert alert-success px-2 my-3"role="alert">
                  Salesperson Succesfully Created!
      </div>
            )}
          </div>
        </div>
      </div>
    );
}

export default CreateSalespeople
