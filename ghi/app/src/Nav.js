import { NavLink } from 'react-router-dom';
import React, { useState } from 'react';

function Nav() {
  const [isOpen, setIsOpen] = useState({
    inventory: false,
    sales: false,
    services: false,
  });

  const toggleDropdown = (category) => {
    setIsOpen({
      ...isOpen,
      [category]: !isOpen[category],
    });
  };

  const closeDropdown = () => {
    setIsOpen({
      inventory: false,
      sales: false,
      services: false,
    });
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
            <li className="nav-item">
              <div className="dropdown">
                <NavLink className="nav-link dropdown-toggle" to="#" onClick={() => toggleDropdown('inventory')}>
                  Inventory
                </NavLink>
                {isOpen.inventory && (
                  <ul className="dropdown-content-horizontal">
            <li><NavLink className="nav-link" to="/automobiles" onClick={closeDropdown}>List of Automobiles</NavLink>
            </li>
              <li><NavLink className="nav-link" to="/automobiles/new" onClick={closeDropdown}>Create Automobile</NavLink>
            </li>
              <li><NavLink className="nav-link" to="/vehicles" onClick={closeDropdown}>List of Models</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/vehicles/new" onClick={closeDropdown}>Create Model</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/manufacturers" onClick={closeDropdown}>List of Manufacturers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/manufacturers/new" onClick={closeDropdown}>Create Manufacturer</NavLink>
            </li>
            </ul>
                )}
            </div>
            <div className="dropdown">
                <NavLink className="nav-link dropdown-toggle" to="#" onClick={() => toggleDropdown('services')}>
                  Services
                </NavLink>
                {isOpen.services && (
                  <ul className="dropdown-content-horizontal">
            <li><NavLink className="nav-link" to="/technicians/list" onClick={closeDropdown}>List of Technicians</NavLink>
            </li>
              <li><NavLink className="nav-link" to="/technicians/new" onClick={closeDropdown}>Create Technician</NavLink>
            </li>
              <li><NavLink className="nav-link" to="/appointments/list" onClick={closeDropdown}>List of Appointments</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/new" onClick={closeDropdown}>Create Appointment</NavLink>
            </li>
            </ul>
                )}
            </div>
            <div className="dropdown">
                <NavLink className="nav-link dropdown-toggle" to="#" onClick={() => toggleDropdown('sales')}>
                  Sales
                </NavLink>
                {isOpen.sales && (
                  <ul className="dropdown-content-horizontal">
            <li><NavLink className="nav-link" to="/customers" onClick={closeDropdown}>List of Customers</NavLink>
            </li>
              <li><NavLink className="nav-link" to="/customers/new" onClick={closeDropdown}>Create Customer</NavLink>
            </li>
              <li><NavLink className="nav-link" to="/salespeople" onClick={closeDropdown}>List of Salespeople</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/salespeople/new" onClick={closeDropdown}>Create Salesperson</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/salespeople/history" onClick={closeDropdown}>Salesperson History</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/sales" onClick={closeDropdown}>List of Sales</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/sales/new" onClick={closeDropdown}>Create Sale</NavLink>
            </li>
            </ul>
                )}
            </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
