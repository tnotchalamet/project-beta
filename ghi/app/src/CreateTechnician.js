import {useEffect, useState} from "react";

function CreateTechnician() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeID] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Technician</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" id="first_name" value={first_name} className="form-control"/>
                        <label htmlFor="first_name">Technician First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" value={last_name} className="form-control"/>
                        <label htmlFor="last_name">Technician Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIDChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" value={employee_id} />
                        <label htmlFor="employee_id">Technician ID</label>
                    </div>
                    <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateTechnician
