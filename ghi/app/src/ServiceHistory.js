import {useEffect, useState} from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);

    const getData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    };

    useEffect(() => {
        getData()
    }, []);

    return(
        <div>
        <h1>Service History</h1>
        <div className="search-bar-container">
            <div>SearchBar</div>
            <div>Search Results</div>
        </div>

            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">VIN</th>
                    <th scope="col">VIP</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Technician</th>
                    <th scope="col">Reason</th>
                    <th></th>
                    <th></th>
                    <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map (appointment => {
                        const formattedDate = new Date(appointment.date_time).toLocaleDateString();
                        const formattedTime = new Date(appointment.date_time).toLocaleTimeString();
                        return (
                            <tr key={appointment.vin}>
                                <td>{appointment.vin}</td>
                                <td>VIP</td>
                                <td>{appointment.customer}</td>
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appointment.first_name} {appointment.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ServiceHistory
