import {useEffect, useState} from "react";

function ListSalespersonHistory(props) {
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([])

    function handleChangeSalesperson(event) {
        const { value } = event.target;
        setSalesperson(value);
      }


      useEffect(() => {
        const salesOf = props.sales.filter(sale => {
            return sale.salesperson.id == salesperson
        })
        setSales(salesOf)
    }, [salesperson]);




    return (
        <div className="my-1">
        <h1 className="display-5 fw-bold">Salesperson History</h1>
        <div className="mb-3">
                        <select value={salesperson} onChange={handleChangeSalesperson} required name="salesperson_id" id="salesperson_id" className="form-select">
                            <option value="">Choose a salesperson</option>
                            {props.salespeople.map(salesperson => {
                                return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Employee</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={ sale.id }>
                <td>{ sale.salesperson.employee_id }</td>
                <td>{ sale.salesperson.first_name} { sale.salesperson.last_name }</td>
                <td>{ sale.customer.first_name} { sale.customer.last_name }</td>
                <td>{ sale.automobile.vin } </td>
                <td>{ sale.price } </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
    }

    export default ListSalespersonHistory
