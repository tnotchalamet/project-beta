import {useEffect, useState} from "react";

function CreateManufacturer({ getManufacturers }) {
    const [name, setName] = useState('');
    const [submitted, setHasSubmitted] = useState(false);

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        name,
      };


    const Url = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }


      const response = await fetch(Url, fetchConfig);
        if (response.ok) {
          setName('');
          setHasSubmitted(true);
          getManufacturers()
        }
      }

    function handleChangeName(event) {
      const { value } = event.target;
      setName(value);
    }


    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (submitted) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleChangeName} placeholder="Manufacturer" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
            {( submitted &&
            <div className="alert alert-success px-2 my-3"role="alert">
                  Manufacturer Succesfully Created!
      </div>
            )}
          </div>
        </div>
      </div>
    );
}

export default CreateManufacturer
