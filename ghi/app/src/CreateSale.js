import {useEffect, useState} from "react";

function CreateSale({props, getSales, getAutomobiles}) {
    const [automobile, setAutomobile] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespersons, setSalespersons] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    const [submitted, setSubmitted] = useState(false)

    async function fetchAutoVO() {
        const url = 'http://localhost:8090/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.automobiles)
        }
    }
    async function fetchSalespeople() {
      const url = 'http://localhost:8090/api/salespeople/';
      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setSalespersons(data.salespersons)
      }
  }
  async function fetchCustomers() {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers)
    }
}

    useEffect(() => {
        fetchAutoVO();
        fetchSalespeople();
        fetchCustomers();
    }, []);



    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
          automobile,
          salesperson,
          customer,
          price,
        };

      const Url = 'http://localhost:8090/api/sales/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        }
    }
        const response = await fetch(Url, fetchConfig)
        if (response.ok) {
          console.log(automobile)
      }
      const id = automobile
      const auto_data = {
        "sold": true
      }
      const auto_url = `http://localhost:8100/api/automobiles/${id}/`;
      const patchConfig = {
          method: "put",
          body: JSON.stringify(auto_data),
          headers: {
            'Content-Type': 'application/json',
          }
        }
        const patchResponse = await fetch(auto_url, patchConfig)
        if (patchResponse.ok) {
          console.log(automobile)
        setAutomobile('');
        setSalesperson('');
        setCustomer('');
        setPrice('');
        setSubmitted(true);
        getSales();
        getAutomobiles();
        }
      }


    function handleChangeAutomobile(event) {
      const { value } = event.target;
      setAutomobile(value);
    }

    function handleChangeSalesperson(event) {
        const { value } = event.target;
        setSalesperson(value);
      }

      function handleChangeCustomer(event) {
        const { value } = event.target;
        setCustomer(value);
      }

      function handleChangePrice(event) {
        const { value } = event.target;
        setPrice(value);
      }





    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
            <select value={automobile} onChange={handleChangeAutomobile} required name="automobile" id="automobile" className="form-select my-3 px-2">
                            <option value="">Choose a vin</option>
                            {automobiles.filter( automobile => !automobile.sold).map(automobile => {
                                return (
                                <option key={automobile.id} value={automobile.id}>
                                    {automobile.vin}
                                </option>
                                );
                            })}
            </select>
            <select value={salesperson} onChange={handleChangeSalesperson} required name="salesperson" id="salesperson" className="form-select my-3 px-2">
            <option value="">Choose a salesperson</option>
            {salespersons.map(salesperson => {
                return (
                <option key={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name} {salesperson.last_name}
                </option>
                );
            })}
            </select>
            <select value={customer} onChange={handleChangeCustomer} required name="customer_id" id="customer_id" className="form-select my-3 px-2">
            <option value="">Choose a customer</option>
            {customers.map(customer => {
                return (
                <option key={customer.id} value={customer.id}>
                    {customer.first_name} {customer.last_name}
                </option>
                );
            })}
            </select>
            <div className="form-floating mb-3 my-2">
<input value={price} onChange={handleChangePrice} placeholder="price" required type="text" name="price" id="price" className="form-control" />
<label htmlFor="price">Price</label>
</div>

              <button className="btn btn-success">Create</button>
            </form>
            {( submitted &&
            <div className="alert alert-success px-2 my-3"role="alert">
                  Sale Succesfully Created!
      </div>
            )}
          </div>
        </div>
      </div>
    );
            }

export default CreateSale
