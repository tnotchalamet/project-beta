import {useEffect, useState} from "react";

const deleteManufacturer = async (id, getManufacturers, setDelete ) => {
    const url = `http://localhost:8100/api/manufacturers/${id}/`
    const fetchConfig = {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        getManufacturers();
        setDelete(true)
    }
};

function ListManufacturers(props) {
    const [deleted, setDelete] = useState(false)
    useEffect(() => {
    },
      [deleted]);
      
return (
    <div className="my-1">
    <h1 className="display-5 fw-bold">Manufacturers</h1>
  <table className="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
      </tr>
    </thead>
    <tbody>
      {props.manufacturers.map(manufacturer => {
        return (
          <tr key={ manufacturer.id }>
            <td>{ manufacturer.name }</td>
            <td><button type="button" onClick={() => deleteManufacturer(manufacturer.id, props.getManufacturers, setDelete)} className="btn btn-dark">
                Delete
              </button>
              </td>
          </tr>
        );
      })}
    </tbody>
  </table>
  <div className="my-5 px-2 container">
    {deleted && (
<div className="alert alert-primary" id="delete-message">
        Manufacturer succesfully deleted.
</div>
    )}
</div>
</div>
);
}

export default ListManufacturers
