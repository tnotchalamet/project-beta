import {useEffect, useState} from "react";

function ListCustomers(props) {
return (
    <div className="my-1">
    <h1 className="display-5 fw-bold">Customers</h1>
  <table className="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Phone Number</th>
        <th>Address</th>
      </tr>
    </thead>
    <tbody>
      {props.customers.map(customer => {
        return (
          <tr key={ customer.id }>
            <td>{ customer.first_name} { customer.last_name }</td>
            <td>{ customer.phone_number }</td>
            <td>{ customer.address }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
</div>
);
}

export default ListCustomers
