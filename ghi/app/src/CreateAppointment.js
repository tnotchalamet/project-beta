import {useEffect, useState} from "react";

function CreateAppointment(){
    const [technicians, setTechnicians] = useState([]);
    const [date_time, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');

    async function fetchData() {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            date_time,
            reason,
            vin,
            customer,
            technician,
        };

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create an Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" value={vin} className="form-control"/>
                            <label htmlFor="picture_url">Automobile VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" id="customer" value={customer} className="form-control"/>
                            <label htmlFor="picture_url">Customer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleDateTimeChange} placeholder="Start Date" required type="datetime-local" name="starts" id="starts" className="form-control" value={date_time} />
                        <label htmlFor="room_count">Appointment Date and Time</label>
                    </div>
                    <div className="mb-3">
                        <select value={technician} onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                            <option value="">Choose a Technician</option>
                            {technicians.map(technician => {
                                return (
                                <option key={technician.id} value={technician.id}>
                                    {technician.first_name} {technician.last_name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleReasonChange} placeholder="Start Date" required type="text" name="reason" id="reason" className="form-control" value={reason} />
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default CreateAppointment
