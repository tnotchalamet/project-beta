function ListSales(props) {
    return (
        <div className="my-1">
        <h1 className="display-5 fw-bold">Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Id</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {props.sales.map(sale => {
            return (
              <tr key={ sale.id }>
                <td>{ sale.salesperson.employee_id }</td>
                <td>{ sale.salesperson.first_name} { sale.salesperson.last_name }</td>
                <td>{ sale.customer.first_name} { sale.customer.last_name }</td>
                <td>{ sale.automobile.vin } </td>
                <td>{ sale.price } </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
    }

    export default ListSales
