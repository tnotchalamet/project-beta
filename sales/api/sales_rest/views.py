from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import Sale, Customer, Salesperson, AutomobileVO
# Create your views here.

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [ "vin", "sold", "id"]

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id", "sold"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "phone_number", "address" , "id"]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "id" ]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "salesperson", "customer", "automobile", "id"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
    }

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "customer", "automobile", "id" ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "sales"]
    encoders = {
        "sales": SaleDetailEncoder(),
    }






@require_http_methods(["GET"])
def api_list_automobiles(request):
     automobiles = AutomobileVO.objects.all()
     return JsonResponse({"automobiles": automobiles}, encoder=AutomobileVOListEncoder, safe=False)

@require_http_methods(["GET", "PUT"])
def api_detail_automobiles(request, pk):
     if request.method == "GET":
        automobile = AutomobileVO.objects.get(id=pk)
        return JsonResponse(automobile, encoder=AutomobileVOListEncoder, safe=False)
     else:
        content = json.loads(request.body)
        automobile = AutomobileVO.objects.filter(id=pk).update(**content)
        return JsonResponse(automobile, encoder=AutomobileVODetailEncoder, safe=False)



@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse({"salespersons": salespersons}, encoder=SalespersonListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(salesperson, encoder=SalespersonListEncoder, safe=False,)


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, encoder=CustomerListEncoder, safe=False,)



@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleListEncoder, safe=False)
    else:
        content = json.loads(request.body)

    try:

        automobile_id = content["automobile"]
        automobile = AutomobileVO.objects.get(id=automobile_id)
        content["automobile"] = automobile

    except AutomobileVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid automobile id"},
            status=400,
        )

    try:

        salesperson_id = content["salesperson"]
        salesperson = Salesperson.objects.get(id=salesperson_id)
        content["salesperson"] = salesperson

    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid salesperson id"},
            status=400,
        )

    try:

        customer_id = content["customer"]
        customer = Customer.objects.get(id=customer_id)
        content["customer"] = customer

    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer id"},
            status=400,
        )


    sale = Sale.objects.create(**content)
    return JsonResponse(sale, encoder=SaleListEncoder, safe=False,)




@require_http_methods(["GET"])
def api_sale_detail(request, pk):
     sale = Sale.objects.get(id=pk)
     return JsonResponse(sale, encoder=SaleDetailEncoder, safe=False)

@require_http_methods(["GET", "DELETE"])
def api_salespeople_detail(request, pk):
    if request.method == "GET":
     salesperson = Salesperson.objects.get(id=pk)
     sales = Sale.objects.filter(salesperson=salesperson)
     sales_data = [{
         "price": sale.price,
         "customer": sale.customer.first_name + " " + sale.customer.last_name,
         "vin": sale.automobile.vin
     } for sale in sales]
     salesperson_data = {
         "fist_name" : salesperson.first_name,
         "last_name" : salesperson.last_name,
         "employee_id": salesperson.employee_id,
         "sales": sales_data
     }
     return JsonResponse(salesperson_data)
    else:
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer_detail(request, pk):
    if request.method == "GET":
     customer = Customer.objects.get(id=pk)
     return JsonResponse(customer, encoder=CustomerDetailEncoder, safe=False)

    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(customer, encoder=CustomerDetailEncoder, safe=False,)
    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
