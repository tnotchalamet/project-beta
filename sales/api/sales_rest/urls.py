from django.urls import path


from .views import api_list_automobiles, api_detail_automobiles, api_list_salespeople, api_list_customers, api_list_sales, api_salespeople_detail, api_sale_detail, api_customer_detail

urlpatterns = [
    path("automobiles/", api_list_automobiles, name="api_list_automobiles"),
    path("automobiles/<int:pk>/", api_detail_automobiles, name="api_detail_automobiles"),
    path("salespeople/<int:pk>/", api_salespeople_detail, name="api_salesperson_detail"),
    path("salespeople/", api_list_salespeople, name="api_list_salespersons"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_customer_detail, name="api_customer_detail"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_sale_detail, name="api_sale_detail"),
    path("automobiles/<int:pk>/", api_detail_automobiles, name="api_update_automobile")
]
