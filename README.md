# CarCar

Team:

Timothy Sayson - Sales Microservice
Tyler Henderson - Service Microservice

## URLS
Front-End: http://localhost:3000/
Service API: http://localhost:/8080/
Sale API: http://localhost:/8090/
Inventory API: http://localhost:8100/

## How to run this app:
- Clone https://gitlab.com/tnotchalamet/project-beta/-/tree/main?ref_type=heads
- Run these commands via the terminal:
    - docker volume create beta-data
    - docker-compose up --build
- Testing Data:
    -

## Service microservice

/api/technicians
- GET
    - Displays a list of technicians
    - Example technician object:
{
    "first_name": "Tyler",
    "last_name": "Henderson",
    "employee_id": "THenderson",
    "id": 1
}
- POST
    - Creates a technician
    - Required information
{
    "first_name":
    "last_name":
    "employee_id":
}

/api/appointments
- GET
    - Displays a list of appointments
    - Example appointment object:
{
    "date_time": "2023-12-19T22:39:00.917366+00:00",
    "reason": "Rotor change",
    "status": "Created",
    "vin": "1C3CC5FB2AN120170",
    "customer": "Chris Kringle",
    "id": 1,
    "first_name": "Tyler",
    "last_name": "Henderson",
    "employee_id": "THenderson"
}
- POST
    - Creates an appointment
    - Required information
{
    "date_time": "2024-11-22",
    "reason": "Rotor change",
    "vin": "1C3CC5FB2AN120170",
    "customer": "Chris Kringle",
    "technician": 1
}
- PUT
    - Allows for access to detailed view of appointments

/api/appointments/:id/cancel/
- PUT
    - Allows the status of the appointment to be updated to "Cancelled" without the deletion of the appointment

/api/appointments/:id/finish/
- PUT
    - Allows the status of the appointment to be updated to "Finished" without the deletion of the
    appointment

- Models:
    - Service models include Technician, Appointment (which includes a foreign key
    to the Technician model) and AutomobileVO. AutomobileVO is an immutable copy of the
    Automobile model inside of the inventory monolith because I am unable to import across
    monoliths.

- Poller:
    - The service poller is set up to allow access to the fields of the Automobile model from the Inventory monolith to the AutomobileVO model inside the Service Monolith


	-------------------------------------------------------------  -------------------------------------------------------------

## Sales microservice
The sales microservice contains four models, a customer, a salesperson, an automobilevo and a sale. The sale is a a model that is dependent to a customer, salesperson and automobilevo.

The automobilevo is a value object that stores data from the inventory api by poller. it specifically polls for the automobile model and both contain a shared property called sold. This property is updated upon creation of a sale, but is given a default value of false upon its own creation.

This functionality ensures that the microservice can have a list of available automobiles to be sold and keep track of previous sales records while differentiating between automobiles that have similar models and manufacturers, but different vins. Ultimately, the I chose the unique id given to objects upon creation to be sole identifier for both the automobiles and automobilevo's

The microservice has one direct interaction with the inventory api's backend
database. When a sale is created in the microservice, it sends a PUT request
to modify the corresponding automobile's "sold" property. This is achieved
by using the unique ID's given to the automobile in the inventory api and its corresponding automobilevo model in the sales api to ensure the integrity of data and that the sold property is consistent between the two models.

		-------------------------------------------------------------

## Customer

API methods and requests :
Customer List -> GET -> http://localhost:8090/api/customers/
Create Customer -> POST -> http://localhost:8090/api/customers/new
Individual Customer -> GET -> http://localhost:8090/api/<int:pk>/
Update Customer Details -> PUT -> http://localhost:8090/api/<int:pk>/

		-------------------------------------------------------------


Creating a customer will require a json body in this format :

    {
	"first_name": "Arthur",
	"last_name": "Bucco",
	"address": "299 Vesuvio Ln. Newark NJ",
	"phone_number": "110-090-1324"
}

The return value for creating a customer :
{
	"first_name": "Arthur",
	"last_name": "Bucco",
	"phone_number": "110-090-1324",
	"address": "299 Vesuvio Ln. Newark NJ",
	"id": 1
}

A list of customers will return this :
{
	"customers": [
		{
			"first_name": "Arthur",
			"last_name": "Bucco",
			"phone_number": "110-090-1324",
			"address": "299 Vesuvio Ln. Newark NJ",
			"id": 1
		},
		{
			"first_name": "Felix",
			"last_name": "Lengyeb",
			"phone_number": "999999999",
			"address": "test",
			"id": 2
		}
	]
}
## Salesperson

API methods and requests :
Salesperson List -> GET -> http://localhost:8090/api/customers/
Create Salesperson -> POST -> http://localhost:8090/api/customers/new

		-------------------------------------------------------------


Creating a salesperson will require a json body in this format :

{
	"first_name": "Carmine",
	"last_name": "Lupertazzi",
	"employee_id": "001"
}
The return value for creating a customer :
{
	"first_name": "Carmine",
	"last_name": "Lupertazzi",
	"employee_id": "001",
	"id": 1
}

A list of salespersons will return this :

{
	"salespersons": [
		{
			"first_name": "Carmine",
			"last_name": "Lupertazzi",
			"employee_id": "001",
			"id": 1
		}
        {
            "first_name": Christopher,
            "last_name": Moltisanti,
            "employee_id": "002",
            "id": 2
        }
	]
}

## Sales

API methods and requests :
List of Sales -> GET -> http://localhost:8090/api/sales/
Create Sale -> POST -> http://localhost:8090/api/sales/

Creating a sale will require a json body in this format :

{
	"price": "$77,214",
	"automobile": 1,
	"salesperson": 1,
	"customer": 1
}
		-------------------------------------------------------------
All properties excluding price are related to the unique id that django gives
to each object once it is created to ensure the integrity of data and the backend.

When a sale is made, a request is also sent to the backend to update the status of
the sold property of the automobile. By sending put requests to the backend, this ensures that integrity of data to reflect on both the front and backend The poller will then update records of automobile vo's as well.

As the POST request is sent to http://localhost:8090/api/sales/,
it is followed by a PUT request sent to http://localhost:8100/api/automobiles/
in succcession of the first request.

		-------------------------------------------------------------
The return value for creating a sale :
{
	"price": "$77,214",
	"salesperson": {
		"first_name": "Carmine",
		"last_name": "Lupertazzi",
		"employee_id": "001",
		"id": 1
	},
	"customer": {
		"first_name": "Arthur",
		"last_name": "Bucco",
		"phone_number": "110-090-1324",
		"address": "299 Vesuvio Ln. Newark NJ",
		"id": 1
	},
	"automobile": {
		"vin": "BTTF1901",
		"id": 1,
		"sold": false
	},
	"id": 1
}

A list of sales will return this :

{
	"price": "$77,214",
	"salesperson": {
		"first_name": "Carmine",
		"last_name": "Lupertazzi",
		"employee_id": "001",
		"id": 1
	},
	"customer": {
		"first_name": "Arthur",
		"last_name": "Bucco",
		"phone_number": "110-090-1324",
		"address": "299 Vesuvio Ln. Newark NJ",
		"id": 1
	},
	"automobile": {
		"vin": "BTTF1901",
		"id": 1,
		"sold": false
	},
	"id": 1

    	"price": "$19,000",
	"salesperson": {
		"first_name": "Christopher",
		"last_name": "Moltisanti",
		"employee_id": "002",
		"id": 2
	},
	"customer": {
		"first_name": "Felix",
		"last_name": "Lengyeb",
		"phone_number": "707-921-3341",
		"address": "277 Pinegrove Ln.",
		"id": 2
	},
	"automobile": {
		"vin": "11535",
		"id": 2,
		"sold": false
	},
	"id": 2
}

## Sales history by Salesperson

API methods and requests :
Sales -> GET -> http://localhost:8090/api/salespeople/<int:pk>/

This functionality is achieved by reinforced filtering and
sending a get request based on the specific salesperson and their history of sales.

		-------------------------------------------------------------

The return value for this request will look like :
{
	"fist_name": "Carmine",
	"last_name": "Lupertazzi",
	"employee_id": "001",
	"sales": [
		{
			"price": "$77,214",
			"customer": "Arthur Bucco",
			"vin": "BTTF1901"
		},
		{
			"price": "$999",
			"customer": "Felix Lengyeb",
			"vin": "TESTVIN"
		},
		{
			"price": "1231231231",
			"customer": "Felix Lengyeb",
			"vin": "TESTVIN5"
		},
		{
			"price": "190909",
			"customer": "Felix Lengyeb",
			"vin": "TEST12"
		}
	]
}
